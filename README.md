# API Inema Clima-tempo
Uma API para consumir dados de temperatura, tempo e vento do banco de dados INEMA.

## Instalação
Instale primeiramente o banco de dados **postgrSQL** que contenha o bando de dados **meioambiente** seguindo a seguinte arquitetura:

- meioambiente (db)
  - painel_ambiental (schema)
    - localidades
    - previsaoclimatologica
    - tipoprevisao
    
Depois execute na pasta do projeto:

```bash
php composer.phar start
```

Para modificar a porta padrão basta modificar o script *start* em *composer.json*

## Utilização
O App está configurado para receber apenas solicitações HTTP do tipo GET com as seguintes rotas

### /cidades
```json
[
  {
    "gid": 214,
    "cod_ibge": "2900702",
    "municipio": "Alagoinhas"
  }, ...
]
```

### cidades/{string}

*Exemplo* **{string}** *= 'Sa'*

*Retorna uma busca por nome com a variável passada entre as localidades suportadas pela API*

```json
[
    {
        "idlocalidade": 194,
        "cod_ibge": "2910800",
        "nome": "Feira de Santana",
        "imagem": "feira-de-santana.jpg"
    },
    {
        "idlocalidade": 192,
        "cod_ibge": "2927408",
        "nome": "Salvador",
        "imagem": "salvador.jpg"
    },
    {
        "idlocalidade": 246,
        "cod_ibge": "2928109",
        "nome": "Santa Maria da Vitória",
        "imagem": "santa-maria-da-vitoria.jpg"
    }
]
```


### dados_painel/{cod_ibge}
```json
{
  "municipio": "Salvador",
  "cod_ibge": "2927408",
  "backgroundImage": "url('img/painel-tempo/background/salvador.jpg')",
  "previsao_hoje": {
    "data": "qua, 27 fev",
    "grupo_clima": "Parcialmente nublado",
    "icone": "img/painel-tempo/icones/parcialmente-nublado.svg",
    "temperatura_min": 26,
    "temperatura_max": 35,
    "iuv": "IUV  ",
    "ventos": [
      {
        "periodo": "Manhã",
        "velocidade_vento": "7.1",
        "direcao_vento": "NE"
      },
      {
        "periodo": "Tarde",
        "velocidade_vento": "6.3",
        "direcao_vento": "NE"
      },
      {
        "periodo": "Noite",
        "velocidade_vento": "6.1",
        "direcao_vento": "NE"
      }
    ]
  },
  "previsao_amanha": {
    "data": "qui, 28 fev",
    "grupo_clima": "Parcialmente nublado",
    "icone": "img/painel-tempo/icones/parcialmente-nublado.svg",
    "temperatura_min": 26,
    "temperatura_max": 34
  },
  "previsao_depois_amanha": {
    "data": "sex,  1 mar",
    "grupo_clima": "Parcialmente nublado",
    "icone": "img/painel-tempo/icones/parcialmente-nublado.svg",
    "temperatura_min": 26,
    "temperatura_max": 34
  }
}
```

### localizacao/{longitude}/{latitude}

*Exemplo* **url** *'localizacao/(neg)38(dot)5007495/(neg)12(dot)932279'*

```json
[
    {
        "cod_ibge": "2927408",
        "municipio": "Salvador"
    }
]
```

Retorna

```json
[
    {
        "cod_ibge": null,
        "municipio": null
    }
]
```

caso não encontre localização correspondente com nenhuma das localidades suportadas