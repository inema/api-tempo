<?php

function get_previsao($i, $array){
    foreach ($array as $key => $value) {
        if($value["idtipoprevisao"] == $i){
            return array(
                "id" => $value["idtipoprevisao"],
                "previsao" => $value["previsao"],
                "icone" => $value["icone"]
            );
        }
    }

    return array(
        "id" => $i,
        "mensagem" => "tipo de previsão não cadastrada"
    );
}

function formatar_data($date){
    setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
    date_default_timezone_set('America/Sao_Paulo');
    return strftime('%a, %e %b', strtotime($date));
}