<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../../vendor/autoload.php';
require '../func/functions.php';

// Instanciando novo App
$app = new \Slim\App;

// config
$host = "172.16.0.41";

// Headers
$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
        ->withHeader('Access-Control-Allow-Origin', '*')
        ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
        ->withHeader('Access-Control-Allow-Methods', 'GET')
        ->withHeader('Content-Type', 'application/json');
});


$app->get('/', function (Request $request, Response $response, array $args) {
    return $response->withJson(array(
        "info" => "API do Painel Ambiental desenvolvido por Filipe Lopes"
    ));
});

// App para consumir a lista de todas as cidades disponíveis no banco de dados
$app->get('/cidades', function (Request $request, Response $response, array $args) {
    global $host;

    try{
        $myPDO = new PDO("pgsql:host=$host;dbname=meioambiente", "painel_ambiental", "painel_ambiental");
        $myPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $myPDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        $sql_query = "SELECT idlocalidade, cod_ibge, nome, imagem FROM painel_ambiental.localidades WHERE selo_intranet = 'true' ORDER BY nome ASC";

        $statement = $myPDO->query($sql_query);

        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        $response = $response->withJson($result);

        return $response;

    }catch(PDOException $e){
        return $response->withJson(array(
            "erro" => $e->getMessage()
        ));
    }
});

// Route para busca de cidade por parte do nome do município
$app->get('/cidades/{q_string}', function (Request $request, Response $response, array $args) {
    global $host;

    $q_string = strtolower($args['q_string']);

    try{
        $myPDO = new PDO("pgsql:host=$host;dbname=meioambiente", "painel_ambiental", "painel_ambiental");
        $myPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $myPDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        $sql_query = "SELECT idlocalidade, cod_ibge, nome, imagem FROM painel_ambiental.localidades WHERE selo_intranet = 'true' AND LOWER(nome) LIKE '%$q_string%' ORDER BY nome ASC";

        $statement = $myPDO->query($sql_query);

        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        $response = $response->withJson($result);

        return $response;

    }catch(PDOException $e){
        return $response->withJson(array(
            "erro" => $e->getMessage()
        ));
    }
});

// Route que retorna localização do usuário utilizando funções do banco, caso não encontrado retornar Salvador
$app->get('/localizacao/{long}/{lat}', function (Request $request, Response $response, array $args) {
    global $host;

    // Transformando (neg) e (dot) em - e . PHP não permite route com esses caracteres
    $longitude = str_replace('(neg)', '-', $args['long']);
    $longitude = floatval(str_replace('(dot)', '.', $longitude));
    $latitude = str_replace('(neg)', '-', $args['lat']);
    $latitude = floatval(str_replace('(dot)', '.', $latitude));

    try{
        $myPDO = new PDO("pgsql:host=$host;dbname=meioambiente", "painel_ambiental", "painel_ambiental");
        $myPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $myPDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

        $sql_query = "SELECT * FROM geobahia_public.fn_get_municipio_coordenada($longitude,$latitude)";

        $statement = $myPDO->query($sql_query);

        $result = $statement->fetchAll(PDO::FETCH_ASSOC);
        $response = $response->withJson($result);

        return $response;

    }catch(PDOException $e){
        return $response->withJson(array(
            "erro" => $e->getMessage()
        ));
    }
});

//Rota GET para captura dos dados para painel do tmepo pelo id da cidade, testar com Paulo Afonso 203 e Ilhéus 199
/*  Entrada: id da localidade
    Saída: {
        id: id,
        localidade: 'nome da localidade',
        previsoes: [ ...lista com previsões a partir de hoje ]
    }
*/
$app->get('/dados_painel/{cod_ibge}', function (Request $request, Response $response, array $args) {
    global $host;

    $cod_ibge = $args['cod_ibge'];

    //Data de hoje para fins de teste, substituir depois pela função date() PHP
    $hoje = date('Y-m-d');

    //Caso tenha algum id prossegue
    if($cod_ibge){
        try{
            $myPDO = new PDO("pgsql:host=$host;dbname=meioambiente", "painel_ambiental", "painel_ambiental");
            $myPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $myPDO->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);

            $result = array();

            // Query para criar array com tipos de previsao e armazenar em variavel
            // $sql_query1 = "SELECT idtipoprevisao, previsao, icone FROM painel_ambiental.tipoprevisao WHERE deletado = false";
            // $statement1 = $myPDO->query($sql_query1);
            // $tipos_previsao = $statement1->fetchAll(PDO::FETCH_ASSOC);

            $indiceUV = ["Baixo", "Baixo", "Moderado", "Moderado", "Moderado", "Alto", "Alto", "Muito alto", "Muito alto", "Muito alto", "Extremo", "Extremo", "Extremo", "Extremo"];
            

            // Query para capturar o nome e id da localidade requerida
            // $sql_query2 = "SELECT idlocalidade, nome FROM painel_ambiental.localidades WHERE idlocalidade = $cod_ibge";
            // $statement2 = $myPDO->query($sql_query2);
            // if($statement2->rowCount() <= 0){
            //     return $response->withJson(array(
            //         "erro" => "Localidade não cadastrada em \"painel_ambiental.localidades\""
            //     ));
            // }
            // Caso exista a localidade adiciona os dados ao array
            // foreach ($statement2->fetchAll(PDO::FETCH_ASSOC) as $row) {
            //     $result["id"] = $row["idlocalidade"];
            //     $result["localidade"] = $row["nome"];
            // }

            $sql_query3 = "SELECT municipio, cod_ibge, tipo_previsao, icone, temperatura_min, temperatura_max, indice_uv, dtc_previsao, imagem FROM painel_ambiental.vw_previsaoclimatologica WHERE cod_ibge = '$cod_ibge' AND dtc_previsao >= '$hoje' ORDER BY dtc_previsao ASC";
            $statement3 = $myPDO->query($sql_query3);

            // Verifica se existe esse id no banco de dados e adiciona às previsoes
            if($statement3->rowCount() > 0){
                foreach ($statement3->fetchAll(PDO::FETCH_ASSOC) as $row) {
                    $result["municipio"] = $row["municipio"];
                    $result["cod_ibge"] = $row["cod_ibge"];
                    $result["backgroundImage"] = "url('img/painel-tempo/background/".$row["imagem"]."')";

                    // Pesquisa ventos
                    $sql_query_ventos = "SELECT periodo, velocidade_vento, direcao_vento FROM painel_ambiental.vw_previsaomaritima WHERE cod_ibge = '$cod_ibge' AND dtc_previsao = '".$row["dtc_previsao"]."' ORDER BY dtc_previsao ASC";
                    $statement_ventos = $myPDO->query($sql_query_ventos);

                    // Adiciona previsao do dia
                    if($row["dtc_previsao"] == $hoje){
                        $result["previsao_hoje"] = array(
                            "data" => formatar_data($row["dtc_previsao"]),
                            "grupo_clima" => $row["tipo_previsao"],
                            "icone" => "img/painel-tempo/icones/".$row["icone"],
                            "temperatura_min" => $row["temperatura_min"],
                            "temperatura_max" => $row["temperatura_max"],
                            "iuv" => "IUV ".$row["indice_uv"]." ".$indiceUV[$row["indice_uv"]],
                            // atualizar depois colocando funcao get_ventos para manha, tarde e noite
                            "ventos" => $statement_ventos->fetchAll(PDO::FETCH_ASSOC)
                        );
                    }
                    
                    $amanha = new DateTime($hoje);
                    $amanha->modify("+1 day");
                    $amanha =$amanha->format("Y-m-d");

                    // Adiciona previsao de amanha
                    if($row["dtc_previsao"] == $amanha){
                        $result["previsao_amanha"] = array(
                            "data" => formatar_data($row["dtc_previsao"]),
                            "grupo_clima" => $row["tipo_previsao"],
                            "icone" => "img/painel-tempo/icones/".$row["icone"],
                            "temperatura_min" => $row["temperatura_min"],
                            "temperatura_max" => $row["temperatura_max"]
                        );
                    }

                    $depois_amanha = new DateTime($hoje);
                    $depois_amanha->modify("+2 days");
                    $depois_amanha =$depois_amanha->format("Y-m-d");

                    // Adiciona previsao de amanha
                    if($row["dtc_previsao"] == $depois_amanha){
                        $result["previsao_depois_amanha"] = array(
                            "data" => formatar_data($row["dtc_previsao"]),
                            "grupo_clima" => $row["tipo_previsao"],
                            "icone" => "img/painel-tempo/icones/".$row["icone"],
                            "temperatura_min" => $row["temperatura_min"],
                            "temperatura_max" => $row["temperatura_max"]
                        );
                    }
                }

                // verifica se houve previsao do dia

            }else{
                return $response->withJson(array(
                    "erro" => "Não existem previsões para esse id/localidade"
                ));
            }

            $response = $response->withJson($result);

            return $response;

        }catch(PDOException $e){
            return $response->withJson(array(
                "erro" => $e->getMessage()
            ));
        }
    }else{
        return $response->withJson(array(
            "erro" => "Parâmetro: \"código do ibge\" não passado"
        ));
    }

    $response = $response->withJson($array);

    return $response;
});


$app->run();